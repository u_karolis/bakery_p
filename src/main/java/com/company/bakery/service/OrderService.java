package com.company.bakery.service;

import com.company.bakery.documents.Customer;
import com.company.bakery.documents.Order;
import com.company.bakery.dto.Product;
import com.company.bakery.repository.CustomerRepository;
import com.company.bakery.repository.OrderRepository;
import com.company.bakery.utils.ListUtils;
import javaslang.collection.List;
import javaslang.control.Option;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


@Service
public class OrderService {

    @Autowired
    OrderRepository orderRepo;
    @Autowired
    CustomerService customerService;
    @Autowired
    CustomerRepository customerRepo;

    public void parseOrderFile(MultipartFile file) {
//fixme customer name Uper/Lower Case
        String fileName = file.getOriginalFilename(); // eg. "Iki_2017.10.10.txt"

        String customerName = StringUtils.substringBefore(fileName, "_");
        String product;
        String qty;
        BufferedReader br;
        String line;
        List<Product> productList = List.empty();
        List<Customer> customerList = ListUtils.listOfAll(customerRepo.findAll());
        Option<Customer> option = customerList.find(customer ->
                (customer.getCustomerName()).equalsIgnoreCase(customerName));
        System.out.println(option.toString());
        if (!option.isDefined()) {
            throw new RuntimeException("Client doesn't exist");
        }
        try {
            InputStream is = file.getInputStream();
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                product = StringUtils.substringBefore(line, ",");
//                    String tempQty = StringUtils.substringAfter(line, ",");
                qty = StringUtils.substringAfter(line, ",");
//                    pricePerUnit = StringUtils.substringAfterLast(line, ",");
                System.out.println(product);
                System.out.println(qty);
                productList = productList.append(new Product(product, Double.parseDouble(qty)));
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        System.out.println("list " + customerName);
        for (Product product1 : productList) {
            System.out.println(product1.getProduct() + product1.getQty());
        }
        System.out.println("--------");
        productList.forEach(System.out::println);
        Order order = new Order();
        order.setCustomer(customerName);
        order.setProducts(productList);

        orderRepo.save(order);

//        InputStream inputStream = file.getInputStream();
//        List<Product> productList = List.empty();
//        String name, qty;
//        StringUtils.substringBefore(customerName, "_");
//        Scanner read = new Scanner(file.getInputStream());
//        read.useDelimiter(",");
//
//        while (read.hasNext()) {
//            name = read.next();
//            qty = read.next();
//            productList.prepend(new Product(name, Double.parseDouble(qty)));
//        }
//
//        Order order = new Order();
//        order.setCustomer(customerName);
//        order.setProducts(productList);
//        orderRepo.save(order);

    }
}
