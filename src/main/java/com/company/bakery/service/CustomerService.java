package com.company.bakery.service;

import com.company.bakery.documents.Customer;
import com.company.bakery.documents.Order;
import com.company.bakery.repository.CustomerRepository;
import com.company.bakery.utils.ListUtils;
import javaslang.collection.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {

    @Autowired
    CustomerRepository customerRepo;

    public Customer createCustomer(Customer customer) {
        Customer customerData = new Customer();
        customerData.setCustomerName(customer.getCustomerName());
        customerData.setAdrress(customer.getAdrress());
        customerData.setCity(customer.getCity());
        return customerRepo.save(customerData);
    }

    public Customer findCustomer(Order order) {
        javaslang.collection.List<Customer> customerList;
        customerList = ListUtils.listOfAll(customerRepo.findAll());
        Customer customer = customerList.filter(it -> it.getCustomerName().equals(order.getCustomer()))
                .getOrElseThrow(() -> new RuntimeException("Costumer doesn't exist"));
        return customer;
        // test
    }

//    public boolean findCustomerByName(String name) {
//        List<Customer> customerList;
//        customerList = ListUtils.listOfAll(customerRepo.findAll());
//        return customerList.find(it ->it.getCustomerName() == name).isDefined();
//    }
}
