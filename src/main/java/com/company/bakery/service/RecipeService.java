package com.company.bakery.service;

import com.company.bakery.documents.Recipe;
import com.company.bakery.dto.Ingredients;
import com.company.bakery.repository.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@Service
public class RecipeService {

    @Autowired
    RecipeRepository recipeRepo;

//    public List<Recipe> getRecipes() {
//        return ListUtils.listOfAll(recipeRepo.findAll());
//    }

    public String createRecipe(Recipe recipe) {
        Ingredients ingredients = new Ingredients();
        Recipe recipeData = new Recipe();
        ingredients.setMilk(recipe.getIngredients().getMilk());
        ingredients.setFlour(recipe.getIngredients().getFlour());
        ingredients.setEgg(recipe.getIngredients().getEgg());
        ingredients.setButter(recipe.getIngredients().getButter());
        ingredients.setOil(recipe.getIngredients().getOil());
        ingredients.setSalt(recipe.getIngredients().getSalt());
        ingredients.setSourCream(recipe.getIngredients().getSourCream());
        ingredients.setSugar(recipe.getIngredients().getSugar());
        ingredients.setWater(recipe.getIngredients().getWater());
        ingredients.setYeast(recipe.getIngredients().getYeast());
        recipeData.setPrice(recipe.getPrice());
        recipeData.setIngredients(ingredients);
        recipeData.setName(recipe.getName());
        recipeData.setHoursBBD(recipe.getHoursBBD());
        recipeRepo.save(recipeData);
        return "Recipe created";
    }


}
