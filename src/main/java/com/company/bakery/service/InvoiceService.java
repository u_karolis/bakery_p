package com.company.bakery.service;

import com.company.bakery.documents.Customer;
import com.company.bakery.documents.Invoice;
import com.company.bakery.documents.Order;
import com.company.bakery.documents.Recipe;
import com.company.bakery.dto.Product;
import com.company.bakery.repository.InvoiceRepository;
import com.company.bakery.repository.OrderRepository;
import com.company.bakery.repository.RecipeRepository;
import com.company.bakery.utils.CostUtils;
import com.company.bakery.utils.ListUtils;
import javaslang.collection.List;
import javaslang.control.Option;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;


@Service
public class InvoiceService {

    @Autowired
    OrderRepository orderRepo;
    @Autowired
    InvoiceRepository invoiceRepo;
    @Autowired
    CustomerService customerService;
    @Autowired
    RecipeRepository recipeRepo;
//FIXME: check if Invoice wasn't created before! We need to check by SMTH
    public void createInvoice(String id) throws IOException {

// seller info
        String sellerName = "Karolis";
        String sellerAdrr = "Kalvariju g. 125";

//Invoice info
        Date date = new Date();
        int invoiceNo = ListUtils.listOfAll(invoiceRepo.findAll()).size() + 1;
        String invoiceLetters = "ABC";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMdd");
        String invoiceNumberDate = dateFormat.format(date);
        String invoiceNumber = invoiceLetters + invoiceNumberDate + invoiceNo;
        System.out.println(id);
// Customer data
        Order order = orderRepo.findOne(id);

        Customer customer = customerService.findCustomer(order);
        String customerName = customer.getCustomerName();
        String customerData = customerName + "\r\n" + customer.getAdrress() + "\r\n" + customer.getCity() + "\r\n";

        List<Product> products = ListUtils.listOfAll(order.getProducts());
        List<Recipe> recipes = ListUtils.listOfAll(recipeRepo.findAll());
// Compare products to recipe list -> find prices -> multiply by product qty -> put hem to costs list
        List<Double> costs = products.map(product -> {
            Option<Recipe> option = recipes.find(recipe -> recipe.getName().equals(product.getProduct()));
            if (option.isDefined()) {
                Recipe recipe = option.get();
                return product.getQty() * recipe.getPrice();
            }
//fixme: throw new excp if product not in recipelist
            return 0.0;
        });

        double total = costs.sum().doubleValue();
//
//        for (double cost : costs) {
//            System.out.println(cost);
//            total += cost;
//        }
        double vat;
        double totalWithVat;


        vat = CostUtils.roundDouble(total * 0.21);
        totalWithVat = CostUtils.roundDouble(vat + total);

// File creation
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd");
        String dateForFile = simpleDateFormat.format(date);

        System.out.println("File destination and name: " + "C:\\Projects\\vcs\\bakery\\src\\main\\resources\\Invoices\\" + customerName + "_" + dateForFile + "_" + invoiceNumber + ".txt");
        String fileName = "C:\\Projects\\vcs\\bakery\\src\\main\\resources\\Invoices\\" + customerName + "_" + dateForFile + "_" + invoiceNumber + ".txt";
        File file = new File(fileName);

//File writer
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            StringBuilder content = new StringBuilder();
            content.append("Invoice\r\n" +
                    invoiceNumber +
                    "\r\n\r\n\r\n" +
                    "SELLER\r\n" +
                    sellerName +
                    "\r\n" +
                    sellerAdrr +
                    "\r\n\r\n\r\n" +
                    "CUSTOMER\r\n" +
                    customerData +
                    "\r\n\r\n\r\n");

//fixme: hoursBBD not by today date BUT by baking date
            products.map(product -> {
                Option<Recipe> option = recipes.find(recipe -> recipe.getName().equals(product.getProduct()));
                if (option.isDefined()) {
                    Recipe recipe = option.get();
                    return content.append(product.getProduct() + " " + product.getQty() + "  " + recipe.getPrice() + "   " +
                            CostUtils.roundDouble(recipe.getPrice() * product.getQty()) + "  Valid until: "
                            + LocalDate.now().plusDays((int) Math.floor(recipe.getHoursBBD()/24 )) +"\r\n");
                }
                throw new RuntimeException("Something went wrong!!!");
            });
            content.append("\r\nTOTAL: " +
                    total +
                    "\r\nVAT: " +
                    vat +
                    "\r\nTOTAL WITH VAT: " +
                    totalWithVat);
            System.out.println(content.toString());
            bw.write(content.toString());
            System.out.println("Done");
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Invoice invoice = new Invoice();
        invoice.setSellerName(sellerName);
        invoice.setCostumerName(customerName);
        invoice.setProducts(products);
        invoice.setTotal(total);
        invoice.setVat(vat);
        invoice.setTotalWithVat(totalWithVat);
        invoice.setVatNumber(invoiceNumber);
        saveInvoice(invoice);
    }


    public void saveInvoice(Invoice invoice) {
        invoiceRepo.save(invoice);
    }
}
