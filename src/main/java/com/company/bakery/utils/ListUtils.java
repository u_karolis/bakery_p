package com.company.bakery.utils;

import javaslang.collection.List;

public class ListUtils {


    public static <T> List<T> listOfAll(Iterable<? extends T> elements) {
        if (elements != null) {
            return List.ofAll(elements);
        }
        return List.empty();
    }
}
