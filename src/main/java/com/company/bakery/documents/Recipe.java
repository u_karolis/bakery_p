package com.company.bakery.documents;


import com.company.bakery.dto.Ingredients;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Document(collection = "recipe")
//@JsonIgnoreProperties(value = {"name"}, allowGetters = true)
public class Recipe {

    @Id
    String id;

    @NotNull
    @Indexed(unique = true)
    private String name;

    //fixme: init date
    @CreatedDate
    private Date createdAt = new Date();

    private int hoursBBD;
    private Ingredients ingredients;
    private boolean visibility;
    //   private String sertificate;
    private double price;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHoursBBD() {
        return hoursBBD;
    }

    public void setHoursBBD(int hoursBBD) {
        this.hoursBBD = hoursBBD;
    }

    public Ingredients getIngredients() {
        return ingredients;
    }

    public void setIngredients(Ingredients ingredients) {
        this.ingredients = ingredients;
    }

    public boolean isVisibility() {
        return visibility;
    }

    public void setVisibility(boolean visibility) {
        this.visibility = visibility;
    }
//
//    public String getSertificate() {
//        return sertificate;
//    }
//
//    public void setSertificate(String sertificate) {
//        this.sertificate = sertificate;
//    }
}
