package com.company.bakery;

import javaslang.jackson.datatype.JavaslangModule;
import org.apache.el.parser.ParseException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;

@SpringBootApplication
public class BakeryApplication {

    public static void main(String[] args) throws IOException, ParseException {
        SpringApplication.run(BakeryApplication.class, args);
    }

    @Bean
    public JavaslangModule jacksonBuilder() {
        return new JavaslangModule();
    }


}