package com.company.bakery.repository;

import com.company.bakery.documents.Customer;
import javaslang.collection.List;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerRepository extends MongoRepository<Customer, String> {
    List<Customer> findByCustomerName(String name);
    Customer findById(String id);
}
