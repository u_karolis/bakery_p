package com.company.bakery.repository;

import com.company.bakery.documents.Recipe;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecipeRepository extends MongoRepository<Recipe, String> {
    Recipe findById(String id);
}
