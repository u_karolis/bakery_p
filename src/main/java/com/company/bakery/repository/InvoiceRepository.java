package com.company.bakery.repository;

import com.company.bakery.documents.Invoice;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface InvoiceRepository extends MongoRepository<Invoice, String>{
}
