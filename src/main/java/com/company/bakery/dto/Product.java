package com.company.bakery.dto;

public class Product {

    private String product;
    private double qty;
//    double pricePerUnit;


    public Product(String product, double qty) {
        this.product = product;
        this.qty = qty;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public double getQty() {
        return qty;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }
}
//
//    public double getPricePerUnit() {
//        return pricePerUnit;
//    }
//
//    public void setPricePerUnit(double pricePerUnit) {
//        this.pricePerUnit = pricePerUnit;
//    }
//}
