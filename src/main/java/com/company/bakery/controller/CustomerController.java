package com.company.bakery.controller;

import com.company.bakery.documents.Customer;
import com.company.bakery.repository.CustomerRepository;
import com.company.bakery.service.CustomerService;
import com.company.bakery.utils.ListUtils;
import javaslang.collection.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

//fixme all links and @Requestmappings  + Autowiring
@RestController
@RequestMapping(value = "/cutomer")
public class CustomerController {
    @Autowired
    CustomerService customerService;
    @Autowired
    CustomerRepository customerRepo;

    @RequestMapping(value = "/getcustomers", method = RequestMethod.GET)
    public List<Customer> getCustomers() {
        System.out.println("getCustomers");
//        Customer customerData = new Customer();
//        customerData.setCustomerName("Iki");
//        customerData.setAdrress("kalvariju 125");
//        customerData.setCity("Vilnius");
//        customerRepo.save(customerData);
//
//        Customer customerData1 = new Customer();
//        customerData1.setCustomerName("Maxima");
//        customerData1.setAdrress("kalvariju 1");
//        customerData1.setCity("Vilnius");
//        customerRepo.save(customerData1);
//
//        Customer customerData2 = new Customer();
//        customerData2.setCustomerName("Aibe");
//        customerData2.setAdrress("Šuniškių pl. 125");
//        customerData2.setCity("Vilnius");
//        customerRepo.save(customerData2);
//
//        Customer customerData3 = new Customer();
//        customerData3.setCustomerName("Uab Barsukas");
//        customerData3.setAdrress("sienu g. 12");
//        customerData3.setCity("Vilnius");
//        customerRepo.save(customerData3);
//        Customer customerData4 = new Customer();
//        customerData4.setCustomerName("Latga");
//        customerData4.setAdrress("Nieko g. 15");
//        customerData4.setCity("Vilnius");
//        customerRepo.save(customerData4);
//        Customer customerData5 = new Customer();
//        customerData5.setCustomerName("Aibe");
//        customerData5.setAdrress("olandu g. 5");
//        customerData5.setCity("Vilnius");
//        customerRepo.save(customerData5);
//        Customer customerData6 = new Customer();
//        customerData6.setCustomerName("Aibe");
//        customerData6.setAdrress("Šuniškių pl. 125");
//        customerData6.setCity("Vilnius");
//        customerRepo.save(customerData6);
        List<Customer> customerList = ListUtils.listOfAll(customerRepo.findAll());
        return customerList;
    }

    @RequestMapping(value = "/getcustomer/{id}", method = RequestMethod.GET)
    public Customer getCustomer(@PathVariable("customerId") String id) {
        System.out.println("getCustomer");
        return customerRepo.findById(id);
    }


    @RequestMapping(value = "/createcustomer", method = RequestMethod.POST)
    public String createCustomer(@Valid @RequestBody Customer customer) {
        System.out.println("createCustomer");
        customerService.createCustomer(customer);
        return "customer created";
    }
}
