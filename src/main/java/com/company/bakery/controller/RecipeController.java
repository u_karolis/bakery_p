package com.company.bakery.controller;


import com.company.bakery.documents.Recipe;
import com.company.bakery.repository.RecipeRepository;
import com.company.bakery.service.RecipeService;
import com.company.bakery.utils.ListUtils;
import javaslang.collection.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

//fixme all links and @Requestmappings  + Autowiring
@RestController
@RequestMapping("/recipe")
@CrossOrigin("*")
public class RecipeController {

    @Autowired
    RecipeRepository recipeRepo;
    @Autowired
    RecipeService recipeService;

    @RequestMapping(value = "/getrecipes", method = RequestMethod.GET)
    public List<Recipe> getRecipes() {
        System.out.println("getRecipes");
        List<Recipe> recipes = ListUtils.listOfAll(recipeRepo.findAll());
        return recipes;
    }

    @RequestMapping(value = "/createrecipe", method = RequestMethod.POST)
    public String createNewRecipe(@Valid @RequestBody Recipe recipe) {
        System.out.println("createNewRecipe");
        recipeService.createRecipe(recipe);
        return "Creating recipe";
    }

    @RequestMapping(value = "/getrecipe/{id}", method = RequestMethod.GET)
    public /*ResponseEntity<Recipe>*/ Recipe getRecipeById(@PathVariable("id") String id) {
        System.out.println("getRecipeById");
        return recipeRepo.findById(id);
//        Ingredients ingredients = new Ingredients();
//        Recipe recipedata = new Recipe();
//        ingredients.setMilk(5);
//        ingredients.setFlour(5);
//        ingredients.setEgg(2);
//        ingredients.setButter(12);
//        recipedata.setPrice(0.14);
//        recipedata.setIngredients(ingredients);
//        recipedata.setName("Bandele 1");
//        recipedata.setHoursBBD(64);
//        recipeRepo.save(recipedata);
//
//        Ingredients ingredients1 = new Ingredients();
//        Recipe recipedata1 = new Recipe();
//        ingredients1.setMilk(5);
//        ingredients1.setFlour(5);
//        ingredients1.setEgg(2);
//        ingredients1.setButter(12);
//        recipedata1.setPrice(0.16);
//        recipedata1.setIngredients(ingredients1);
//        recipedata1.setName("Bandele 2");
//        recipedata1.setHoursBBD(32);
//        recipeRepo.save(recipedata1);
//
//        Ingredients ingredients2 = new Ingredients();
//        Recipe recipedata2 = new Recipe();
//        ingredients2.setMilk(5);
//        ingredients2.setFlour(5);
//        ingredients2.setEgg(2);
//        ingredients2.setButter(12);
//        recipedata2.setPrice(0.17);
//        recipedata2.setIngredients(ingredients2);
//        recipedata2.setName("Bandele 3");
//        recipedata2.setHoursBBD(72);
//        recipeRepo.save(recipedata2);
//
//        Ingredients ingredients3 = new Ingredients();
//        Recipe recipedata3 = new Recipe();
//        ingredients3.setMilk(5);
//        ingredients3.setFlour(5);
//        ingredients3.setEgg(2);
//        ingredients3.setButter(12);
//        recipedata3.setPrice(0.18);
//        recipedata3.setIngredients(ingredients3);
//        recipedata3.setName("Bandele 4");
//        recipedata3.setHoursBBD(72);
//        recipeRepo.save(recipedata3);
//
//        Ingredients ingredients4 = new Ingredients();
//        Recipe recipedata4 = new Recipe();
//        ingredients4.setMilk(5);
//        ingredients4.setFlour(5);
//        ingredients4.setEgg(2);
//        ingredients4.setButter(12);
//        recipedata4.setPrice(0.19);
//        recipedata4.setIngredients(ingredients4);
//        recipedata4.setName("Bandele 5");
//        recipedata4.setHoursBBD(100);
//        recipeRepo.save(recipedata4);

//        Recipe recipe = recipeRepo.findOne(id);
//        if (recipe == null) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        } else {
//            return new ResponseEntity<>(recipe, HttpStatus.OK);
//        }

    }

    //fixme Do i need this???
    @PutMapping(value = "/updaterecipe/{id}")
    public ResponseEntity<Recipe> updateRecipe(@PathVariable("id") String id,
                                               @Valid @RequestBody Recipe recipe) {
        Recipe recipedata = recipeRepo.findOne(id);
        if (recipedata == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        recipedata.setName(recipe.getName());
        recipedata.setHoursBBD(recipe.getHoursBBD());
        recipedata.setVisibility(recipe.isVisibility());
        recipedata.setIngredients(recipe.getIngredients());
        Recipe updatedRecipe = recipeRepo.save(recipedata);
        return new ResponseEntity<>(updatedRecipe, HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleterecipe/{id}")
    public void deleteRecipe(@PathVariable("id") String id) {
        System.out.println("deleteRecipe");
        recipeRepo.delete(id);
    }


}
