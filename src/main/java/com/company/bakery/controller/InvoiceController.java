package com.company.bakery.controller;

import com.company.bakery.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
//fixme all links and @Requestmappings  + Autowiring
@RestController
@RequestMapping(value = "/invoice")
public class InvoiceController {

    @Autowired
    InvoiceService invoiceService;

    @RequestMapping(value = "/createinvoice/{id}", method = RequestMethod.POST)
    public void createInvoice(@PathVariable("id") String id) throws IOException {
        System.out.println("createInvoice");
     invoiceService.createInvoice(id);
    }

}
