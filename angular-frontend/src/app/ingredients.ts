export class ingredients {

   flour: number;
   milk: number;
   butter: number;
   egg: number;
   water: number;
   yeast: number;
   sugar: number;
   salt: number;
   oil: number;
   sourCream: number;
}
