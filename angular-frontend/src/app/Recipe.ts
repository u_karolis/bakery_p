export class Recipe {

  id: string;
  name: string;
  ingredients: object;
  hoursBBD: number;
  visibility: boolean;
  // sertificate: string;
  // createdAt: Date;

}
