import { Injectable } from '@angular/core';
import { Recipe } from './Recipe';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class RecipeService {
  private baseUrl = 'http://localhost:8080';


  constructor(private http: Http) {
  }


  getRecipes(): Promise<Recipe[]> {
    return this.http.get(this.baseUrl + '/recipe/getrecipes/')
      .toPromise()
      .then(response => response.json() as Recipe[])
      .catch(this.handleError);
  }

  createRecipe(recipeData: Recipe): Promise<Recipe> {
    return this.http.post(this.baseUrl + '/recipe/createrecipe/',   recipeData)
      .toPromise().then(response => response.json() as Recipe)
      .catch(this.handleError);
  }

  updateRecipe(recipeData: Recipe): Promise<Recipe> {
    return this.http.put(this.baseUrl + '/recipe/updaterecipe/' + recipeData.id, recipeData)
      .toPromise()
      .then(response => response.json() as Recipe)
      .catch(this.handleError);
  }
  deleteRecipe(id: string): Promise<any> {
    return this.http.delete(this.baseUrl + '/recipe/deleterecipe/' + id)
      .toPromise()
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Some error occured', error);
    return Promise.reject(error.message || error);
  }
}
