import {Component, Input, OnInit} from '@angular/core';
import {RecipeService} from "./recipe.service";
import {Recipe} from './Recipe';
import {NgForm} from '@angular/forms';



@Component({
  selector: 'Recipe-list',
  templateUrl: './Recipe-list.component.html',

})


export class RecipeListComponent implements OnInit {
  recipes: Recipe[];
  newRecipe: Recipe = new Recipe();
  editing: boolean = false;
  editingRecipe: Recipe = new Recipe();

  constructor(private recipeService: RecipeService) {
  }

  ngOnInit(): void {
    this.getRecipes();
  }

  getRecipes(): void {
    this.recipeService.getRecipes()
      .then(recipes => this.recipes = recipes)

  }

  createRecipe(RecipeForm: NgForm): void {
    this.recipeService.createRecipe(this.newRecipe)
      .then(createRecipe => {
        RecipeForm.reset();
        this.newRecipe = new Recipe();
        this.recipes.unshift(createRecipe)
      });
  }


  updateRecipe(recipeData: Recipe): void {
    console.log(recipeData);
    this.recipeService.updateRecipe(recipeData)
      .then(updatedRecipe => {
        let existingRecipe = this.recipes.find(recipe => recipe.id === updatedRecipe.id);
        Object.assign(existingRecipe, updatedRecipe);
        this.clearEditing();
      });
  }

  deleteRecipe(id: string): void {
    this.recipeService.deleteRecipe(id)
      .then(() => {
        this.recipes = this.recipes.filter(recipe => recipe.id != id)
      });
  }


  toggleVisibility(recipeData: Recipe): void {
    recipeData.visibility = !recipeData.visibility;
    this.recipeService.updateRecipe(recipeData)
      .then(updatedRecipe => {
          let existingRecipe = this.recipes.find(recipe => recipe.id == updatedRecipe.id)
          Object.assign(existingRecipe, updatedRecipe)
        }
      );
  }

  editRecipe(recipeData: Recipe): void {
    this.editing = true;
    Object.assign(this.editingRecipe, recipeData)
  }

  clearEditing(): void {
    this.editingRecipe = new Recipe();
    this.editing = false;
  }
}
